#!/usr/bin/env bash
set -euo pipefail

function test1 {
    return 1;
}

function test2 {
    return 0;
}

if test1 || test2; then
    echo 'All good'
    exit 0
else
    echo 'Uh oh'
    exit 1
fi
